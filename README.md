# @richard.martens/react-toys

> ReactJS Controls

[![NPM](https://img.shields.io/npm/v/@richard.martens/react-toys.svg)](https://www.npmjs.com/package/@richard.martens/react-toys) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @richard.martens/react-toys
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from '@richard.martens/react-toys'
import '@richard.martens/react-toys/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [https://www.bitbucket.org/richard-martens/react-toys.git](https://github.com/https://www.bitbucket.org/richard-martens/react-toys.git)
