import React from 'react'

import { ExampleComponent } from '@richard.martens/react-toys'
import '@richard.martens/react-toys/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
